const fs = require('fs');

// Create a readStream
function readStream(src, coding = 'utf8') {
  const rs = fs.createReadStream(src, coding);
  rs.on('error', (err) => console.log(err.message));
  return rs;
}

// Read a file synchronously
function read(src, coding = 'utf8') {
  try {
    return fs.readFileSync(src, coding);
  } catch (error) {
    console.log(`Unable to read file ${src}`);
    return '';
  }
}

// Create a writeStream
function writeFile(dest, data) {
  fs.writeFile(dest, data, (err) => {
    if (err) console.log(err.message);
  });
}

// Make a copy of a file
function copy(src, ext = 'bak') {
  const regex = /\.[a-zA-z0-9]*$/;
  let dest = '';

  // define name/destination of copy
  if (regex.test(src)) {
    dest = src.replace(regex, `.${ext}`);
  } else {
    dest = `${src}.${ext}`;
  }
  // eslint-disable-next-line node/no-unsupported-features/node-builtins
  return fs.promises
    .copyFile(src, dest)
    .then(() => console.log(`INFO: a copy of ${src} was made in ${dest}\n`))
    .catch(() => console.log(`${src} couldn't be copied`));
}

module.exports.copy = copy;
module.exports.read = read;
module.exports.readStream = readStream;
module.exports.writeFile = writeFile;
