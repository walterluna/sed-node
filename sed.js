const files = require('./files');
const parameters = require('./parameters');

// Make a substitution
function substitute(params) {
  // Open a file as a stream
  const readfile = files.readStream(params.input);
  // Create regex patterns to match ocurrences
  const commands = [];
  params.patterns.forEach((pattern) => {
    if (pattern.options.includes('g')) {
      commands.push({
        regex: new RegExp(`${pattern.find}`, 'g'),
        sub: pattern.sub,
      });
    } else {
      commands.push({
        regex: new RegExp(`${pattern.find}`),
        sub: pattern.sub,
      });
    }
  });

  // Set listener for every time te buffer returns data from file
  readfile.on('data', (data) => {
    // Split buffer into lines, evaluate every regex and output result
    const lines = data.split('\n');
    lines.forEach((line) => {
      let nl = line;
      commands.forEach((command) => {
        nl = nl.replace(command.regex, command.sub);
      });
      if (params.output) {
        console.log(nl);
      }
    });
  });
}

// Makes a substitution in place
async function substituteInPlace(params) {
  // Make a copy of file with given extension
  await files.copy(params.input, params.inPlace);

  // Create regex patterns to match occurrences
  const commands = [];
  params.patterns.forEach((pattern) => {
    if (params.pattern.options.includes('g')) {
      commands.push({
        regex: new RegExp(`${pattern.find}`, 'g'),
        sub: pattern.sub,
      });
    } else {
      commands.push({
        regex: new RegExp(`${pattern.find}`),
        sub: pattern.sub,
      });
    }
  });

  // Open a file as a stream
  const readfile = files.readStream(params.input);

  // Set listener for every time the buffer returns data from the file
  let buffer = '';
  readfile.on('data', (data) => {
    // Split buffer into lines, evaluate every regex and output result
    const lines = data.split('\n');
    lines.forEach((line) => {
      let nl = line;
      commands.forEach((command) => {
        nl = nl.replace(command.regex, command.sub);
      });
      if (params.output) {
        console.log(nl);
      }
      buffer += `${nl}\n`;
    });
  });

  // Set listener to overwrite file with substitution upon closing
  readfile.on('close', () => {
    files.writeFile(params.input, buffer);
  });
}

const args = parameters.parse(process.argv.slice(2));

// Check args and run substitution
if (args.unexpected) {
  console.log('Correct command and try again');
} else if (!args.input) {
  console.log('No read file specified');
} else if (args.patterns.length === 0) {
  console.log(
    "Insert a valid pattern. e.g: 's/foo/bar/[OPTIONS]'\nAvailable commands: s\nAvailable options: g, p"
  );
} else if (args.inPlace) {
  // Substitution in place
  substituteInPlace(args);
} else {
  // Basic substitution
  substitute(args);
}
