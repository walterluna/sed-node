# sed node

This is a simple and naive version of sed built with nodejs


Sed is a Unix utility used to parse and transform text. Reference: https://archive.flossmanuals.net/command-line/sed.html
For this assignment a simple and naive version of sed will be created.
This requirements will be:

- Only the substitution command is required.
- Create a command line utility that accepts options and arguments.
- The first argument will be the command and the second one will be the file.
- Check if the file specified exists.
- Check if the substitution command is valid.
- Implement the -n option, which prevents a line from being printed unless specified by ‘p’.
- Implement the -i [extension] which tells sed to edit the file instead of printing to the standard output.
- A copy of the original should be saved in [file_name].[extension]
- Multiple substitution commands should be accepted with the -e option, with one command per -e appearance.
- When the -n option appears, the line should not be printed to the standard output unless specified.
- The -f [script-file] option should expect a file which consists of several lines containing one command each.
- Implement the p flag which tells that the line should be printed.
- Implement the g flag which tells that all occurrences of the search should be replaced on the line. By default only the first occurrence is replaced.
 

NOTE: You can use this online tool, to get a feel of how sed works if you are not using a unix machine: https://sed.js.org/