const files = require('./files');

function parse(arr) {
  const args = {
    quiet: false,
    inPlace: false,
    scriptFile: false,
    scripts: 0,
    patterns: [],
    input: false,
    unexpected: false,
  };

  arr.forEach((item) => {
    if (item === '-n') {
      // -n flag to show no output
      args.quiet = true;
    } else if (item === '-e') {
      // -e flag for multiple substitution commands
      args.scripts += 1;
    } else if (/^-i\w*$/.test(item)) {
      // -i flag to subsitute in place, stores copy file extension
      args.inPlace = item.replace(/^-i/, '');
    } else if (/^-f(\w)*(\.\w*)*$/.test(item)) {
      // -f flag to submit a file with substitution commands
      args.scriptFile = item.replace(/^-f/, '');
      let script = files.read(args.scriptFile);
      script = script.replace("'", '');
      script = script.split('\n');
      script.forEach((rawLine) => {
        const line = rawLine.replace(new RegExp("'", 'g'), '');
        if (/(s|S)+\/\w*\/\w*\/(p|P|g|G){0,2}$/.test(line)) {
          args.patterns.push(line.split('/'));
          args.scripts += 1;
        } else if (line === '') {
          // ignore empty lines
        } else {
          console.log(`Unexpected token in file ${args.scriptFile}: ${line}`);
          args.unexpected = true;
        }
      });
    } else if (/(s|S)+\/\w*\/\w*\/(p|P|g|G){0,2}$/.test(item)) {
      // substitution command(s)
      if (args.scripts === 0 || args.patterns.length + 1 === args.scripts) {
        args.patterns.push(item.split('/'));
      } else {
        args.unexpected = true;
        console.log(`Unexpected token: ${item}`);
        console.log('Use -e to evalute multiple substitution commands');
      }
    } else if (/^\.?\w*(\.\w*)*$/.test(item)) {
      // input file
      args.input = item;
    } else {
      // unexpected tokens handling
      args.unexpected = true;
      console.log(`Unexpected token: ${item}`);
    }
  });

  const tmpPatterns = [];
  args.patterns.forEach((pattern) => {
    tmpPatterns.push({
      command: pattern[0].toLowerCase(),
      find: pattern[1],
      sub: pattern[2],
      options: pattern[3].toLowerCase(),
    });
  });
  args.patterns = tmpPatterns;

  args.output = !(args.quiet && !args.patterns[0].options.includes('p'));
  return args;
}

module.exports.parse = parse;
